<?php

namespace Tigren\TestPreference\Block\Product;

/**
 * Class View
 * @package Tigren\TestPreference\Block\Product
 */
class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * @return \Magento\Catalog\Model\Product|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct()
    {
        // logging to test override
        $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug('Block Override Test Preference');

        if (!$this->_coreRegistry->registry('product') && $this->getProductId()) {
            $product = $this->productRepository->getById($this->getProductId());
            $this->_coreRegistry->register('product', $product);
        }
        return $this->_coreRegistry->registry('product');
    }
}