<?php

namespace Tigren\TestPlugin\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Index
 * @package Tigren\TestPlugin\Controller\Index
 */
class Index extends Action
{
    /**
     * @var
     */
    protected $title;

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
//        die("aaaa00");
        echo $this->setTitle('Welcome');
        echo $this->getTitle();
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return mixed
     */
    public function setTitle($title)
    {
        return $this->title = $title;
    }
}
