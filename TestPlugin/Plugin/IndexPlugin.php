<?php

namespace Tigren\TestPlugin\Plugin;

use Tigren\TestPlugin\Controller\Index\Index;

/**
 * Class IndexPlugin
 * @package Tigren\TestPlugin\Plugin
 */
class IndexPlugin
{
    /**
     * @param Index $subject
     * @param $title
     * @return array
     */
    public function beforeSetTitle(Index $subject, $title)
    {
        $title = $title . " to ";
        echo __METHOD__ . "</br>";

        return [$title];
    }

    /**
     * @param Index $subject
     * @param $result
     * @return string
     */
    public function afterGetTitle(Index $subject, $result)
    {
        echo __METHOD__ . "</br>";

        return '<h1>' . $result . 'Tigren.com' . '</h1>';
    }

    /**
     * @param Index $subject
     * @param callable $proceed
     * @return mixed
     */
    public function aroundGetTitle(Index $subject, callable $proceed)
    {
        echo __METHOD__ . " - Before proceed() </br>";
        $result = $proceed();
        echo __METHOD__ . " - After proceed() </br>";

        return $result;
    }
}
