<?php

namespace Tigren\TestPlugin\Plugin;

use Closure;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;

/**
 * Class ProductPlugin
 * @package Tigren\TestPlugin\Plugin
 */
class ProductPlugin
{
    /**
     * @param Product $subject
     * @param $result
     * @return string
     */
    public function afterGetName(Product $subject, $result)
    {
        // logging to test override
        $logger = ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug(__METHOD__ . ' - ' . __LINE__);
        return $result . " + modified by after-Plugin";
    }

    /**
     * @param View $subject
     */
    public function beforeGetProduct(View $subject)
    {
        // logging to test override
        $logger = ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug(__METHOD__ . ' - ' . __LINE__);
    }

    /**
     * @param View $subject
     * @param $result
     * @return mixed
     */
    public function afterGetProduct(View $subject, $result)
    {
        // logging to test override
        $logger = ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug(__METHOD__ . ' - ' . __LINE__);

        return $result;
    }

    /**
     * @param View $subject
     * @param Closure $proceed
     * @return mixed
     */
    public function aroundGetProduct(View $subject, Closure $proceed)
    {
        // logging to test override
        $logger = ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug(__METHOD__ . ' - ' . __LINE__);

        // call the core observed function
        $returnValue = $proceed();

        // logging to test override
        $logger->debug(__METHOD__ . ' - ' . __LINE__);

        return $returnValue;
    }

    /**
     * @param Closure $proceed
     * @return mixed
     */
    public function aroundExecute(Closure $proceed)
    {
        // logging to test override
        $logger = ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
        $logger->debug(__METHOD__ . ' - ' . __LINE__);

        // call the core observed function
        $returnValue = $proceed();

        // logging to test override
        $logger->debug(__METHOD__ . ' - ' . __LINE__);

        return $returnValue;
    }

}
