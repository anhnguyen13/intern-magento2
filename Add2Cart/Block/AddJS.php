<?php

namespace Tigren\Add2Cart\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class AddJS
 * @package Tigren\Add2Cart\Block
 */
class AddJS extends Template
{
    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * AddJS constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }
}
