/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        '*': {
            ajaxAdd2Cart: 'Tigren_Add2Cart/js/ajax-add-to-cart'
        }
    },
};