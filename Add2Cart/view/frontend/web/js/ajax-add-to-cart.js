/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/translate',
    'underscore',
    'Magento_Catalog/js/product/view/product-ids-resolver',
    'jquery-ui-modules/widget',
    'Magento_Ui/js/modal/modal'
], function ($, $t, _, modal) {
    'use strict';

    $.widget('mage.ajaxAdd2Cart', {
        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: '',
            modalForm: '#modal-content',
            checkoutCartUrl: null,
            continuteButtonSelector: '.button.tg-btn-left.continue',
            viewCartButtonSelector:'.button.tg-btn-right.view-cart',
            checkoutButtonSelector: '[data-role="proceed-to-checkout"]'
        },

        /** @inheritdoc */
        _create: function () {
            if (this.options.bindSubmit) {
                this.options.modalOption = this._getModalOptions();
                this._bindSubmit();
            }
        },

        /**
         * @private
         */
        _bindSubmit: function () {
            var self = this;

            if (this.element.data('ajax-addtocart-initialized')) {
                return;
            }

            this.element.data('catalog-addtocart-initialized', 1);
            this.element.on('submit', function (e) {
                e.preventDefault();
                $('body').trigger('processStart');
                var form = $(this).closest('form');

                if (form.length !== 0) {
                    var actionUrl = form.attr('action'),
                        additionUrl = "";
                    if (actionUrl.search('checkout/cart/add') !== -1) {
                        additionUrl = 'http://magento2.localhost.com/ajaxcart/cart/showpopup'
                    }
                }
                var params = new FormData(this);
                // console.log(params);
                self.showPopup(params, additionUrl);
            });
        },

        showPopup: function (params, additionalUrl) {
            var self = this,
                modalOption = this.options.modalOption,
                modalForm = this.options.modalForm;

            $.ajax(
                {
                    url: additionalUrl,
                    data: params,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        if (self.isLoaderEnabled()) {
                            $('body').trigger(self.options.processStart);
                        }
                    },
                    success: function (res) {
                        if (self.isLoaderEnabled()) {
                            $('body').trigger(self.options.processStop);
                        }
                        // if (res.backUrl) {
                        //     window.location = res.backUrl;
                        //     return;
                        // }

                        console.log(res.product);
                        $('body').trigger('processStop');

                        //Initialize modal
                        $(modalForm).modal(modalOption);
                        $(self.options.modalForm).html(res.html_popup);

                        //open modal
                        $(modalForm).trigger('openModal');

                    },
                    error: function (response) {

                    }
                }
            );
        },

        _getModalOptions: function () {
            return {
                type: 'popup',
                responsive: true,
                // title: 'Product detail',
                // buttons: [{
                //     text: $.mage.__('Continue'),
                //     class: '',
                //     click: function () {
                //         this.closeModal();
                //     }
                // }]
            };
        },

        /**
         * @private
         */
        _redirect: function (url) {
            let urlParts, locationParts, forceReload;

            urlParts = url.split('#');
            locationParts = window.location.href.split('#');
            forceReload = urlParts[0] === locationParts[0];

            window.location.assign(url);

            if (forceReload) {
                window.location.reload();
            }
        },

        /**
         * @return {Boolean}
         */
        isLoaderEnabled: function () {
            return this.options.processStart && this.options.processStop;
        }
    });

    return $.mage.ajaxAdd2Cart;
});
