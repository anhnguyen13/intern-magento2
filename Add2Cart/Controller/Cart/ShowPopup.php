<?php

namespace Tigren\Add2Cart\Controller\Cart;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Zend_Filter_LocalizedToNormalized;

/**
 * Class ShowPopup
 * @package Tigren\Add2Cart\Controller\Cart
 */
class ShowPopup extends \Magento\Checkout\Controller\Cart\Add
{
    protected $jsonFactory;
    protected $registry;
    private $resultLayoutFactory;

    /**
     * ShowPopup constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param JsonFactory $jsonFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        JsonFactory $jsonFactory,
        LayoutFactory $resultLayoutFactory,
        Registry $registry
    ) {
        $this->jsonFactory = $jsonFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->registry = $registry;
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $productRepository
        );
    }

    /**
     * @return string
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

//        $product= $this->getRequest()->getParam('product');
//        var_dump($params);
//        die("aaaaa");
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get(
                        ResolverInterface::class
                    )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
//            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                return $this->goBack();
            }
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            $imageHelper  = $objectManager->get('\Magento\Catalog\Helper\Image');
//            $image_url = $imageHelper->init($product, 'product_page_image_small')->setImageFile($product->getFile())->getUrl();

            $this->registry->register('product', $product);
            $this->cart->addProduct($product, $params);
            $this->cart->save();
//            if (!empty($related)) {
//                $this->cart->addProductsByIds(explode(',', $related));
//            }

            $layout = $this->resultLayoutFactory->create(['cacheable' => false]);
            $layout->getUpdate()->load(['ajaxcart_cart_showpopup']);
            $layout->generateXml();
            $layout->generateElements();
            $html = $layout->getOutput();
            $result['html_popup'] = $html;

            $this->getResponse()->representJson(
                $this->_objectManager->get(Data::class)->jsonEncode($result)
            );
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t add this item to your shopping cart right now.')
            );
            $this->_objectManager->get(LoggerInterface::class)->critical($e);
            return $this->goBack();
        }
    }
}
