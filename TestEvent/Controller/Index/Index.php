<?php

namespace Tigren\TestEvent\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\DataObject;

/**
 * Class Index
 * @package Tigren\TestEvent\Controller\Index
 */
class Index extends Action
{
    /**
     * Index constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $textDisplay = new DataObject(['text' => 'Test Event']);
        $this->_eventManager->dispatch('tigren_testevent_display_text', ['tigren_text' => $textDisplay]);
        echo $textDisplay->getText();
        exit;
    }
}
