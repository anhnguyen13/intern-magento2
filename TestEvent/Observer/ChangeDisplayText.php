<?php

namespace Tigren\TestEvent\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ChangeDisplayText
 * @package Tigren\TestEvent\Observer
 */
class ChangeDisplayText implements ObserverInterface
{

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $displayText = $observer->getData('tigren_text');
        echo $displayText->getText() . " - Event </br>";
        $displayText->setText('Execute event successfully.');
//        die($displayText->getText());
        return $this;
    }
}